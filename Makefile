ifdef IN_NIX_SHELL
include Makefile.nix
endif

.PHONY: all
all:
	cp node_modules/bootstrap/dist/css/bootstrap.min.css static/css/.
	cp node_modules/bootstrap/dist/css/bootstrap.min.css.map static/css/.
	cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map static/js/.
	cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js static/js/.
	cp -a node_modules/leaflet/dist/images/. static/css/images/
	cp node_modules/leaflet/dist/leaflet.js static/js/
	cp node_modules/leaflet/dist/leaflet.js.map static/js/
	cp node_modules/leaflet/dist/leaflet.css static/css/
	cp node_modules/alpinejs/dist/cdn.min.js static/js/
	cp node_modules/chart.js/dist/chart.umd.js static/js/.
	cp node_modules/chartjs-plugin-crosshair/dist/chartjs-plugin-crosshair.min.js static/js/.
