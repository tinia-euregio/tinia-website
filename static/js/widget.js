class EuregioMeteoWidget extends HTMLElement {
  connectedCallback() {
    const id = this.getAttribute('id')
    const lang = this.getAttribute('lang')
    const url = `https://meteo.report${lang !== 'en' ? '/' + lang : ''}/widgets/${id}/`
    fetch(url)
      .then(response => response.html())
      .then(html => {
        const shadow = this.attachShadow({ mode: 'closed' });
        shadow.innerHTML = html;
      })
  }
}
 
window.customElements.define(
  'euregio-meteo-widget',
  EuregioMeteoWidget
)
