#!/usr/bin/env python3
# coding=utf-8

import argparse
import json
import os


def rearrange_neighbors(neighbors, venues_2, venues_3, venues_6):
    for neighboring_town in neighbors["towns"]:
        town = next(item for item in venues_2 if item["id"] == neighboring_town)
        neighbors["towns"][neighboring_town] = {
            "id": neighboring_town,
            "dist": neighbors["towns"][neighboring_town],
            "name": town["name"],
        }
    for neighboring_station in neighbors["stations"]:
        station = next(item for item in venues_3 if item["id"] == neighboring_station)
        neighbors["stations"][neighboring_station] = {
            "id": neighboring_station,
            "dist": neighbors["stations"][neighboring_station],
            "name": station["name"],
        }
    for neighboring_webcam in neighbors["webcams"]:
        webcam = next(item for item in venues_6 if item["id"] == neighboring_webcam)
        neighbors["webcams"][neighboring_webcam] = {
            "id": neighboring_webcam,
            "dist": neighbors["webcams"][neighboring_webcam],
            "name": webcam["name"],
            "has_history": webcam["webcam"]["has_history"],
            "link": webcam["webcam"]["link"]
        }
    neighbors["towns"] = list(neighbors["towns"].values())
    neighbors["stations"] = list(neighbors["stations"].values())
    neighbors["webcams"] = list(neighbors["webcams"].values())


current_directory = os.getcwd()

os.makedirs("content/widget", exist_ok=True)
os.makedirs("content/forecast", exist_ok=True)
os.makedirs("content/observation", exist_ok=True)
os.makedirs("content/webcam", exist_ok=True)

parser = argparse.ArgumentParser()
parser.add_argument(
    "--dev", action="store_true", help="generate only 3 x 2 dynamic pages per type"
)
args = parser.parse_args()

default_towns = [
    "c4efef40-bb6f-4cd3-a982-4e7579da701e",
    "e0c04e2d-8221-48d3-92b9-56e26e743213",
    "5d9e12bb-7274-483e-9acd-44bfdcb916e5",
]
default_stations = [
    "2fb599de-dcc7-4424-8410-c7205be0e6e3",
    "65a91ab5-032b-40ea-a03f-42773d60ec34",
    "f439b2f0-c5b9-43f4-b058-f407f7c99141",
]
default_webcams = [
    "72eb53f3-2596-4eaa-8f17-6ea6b51f2a8c", 
    "ac299f7e-78e9-4d59-9efa-ddf4cfc94c14", 
    "2eb79e42-6855-47bd-b093-c267628b3754",
]

for lang in ["de", "en", "it"]:
    with open(f"data/venues/{lang}/2.json", "r") as json_file:
        venues_2 = json.load(json_file)
    with open(f"data/venues/{lang}/3.json", "r") as json_file:
        venues_3 = json.load(json_file)
    with open(f"data/venues/{lang}/6.json", "r") as json_file:
        venues_6 = json.load(json_file)
    with open(f"data/venues/{lang}/7.json", "r") as json_file:
        venues_7 = json.load(json_file)
    for t in venues_2:
        if args.dev:
            if t["id"] not in default_towns:
                continue
        rearrange_neighbors(t["neighbors"], venues_2, venues_3, venues_6)
        front_matter = {"layout": "widget", "town": t}
        if lang == "en":
            file_name = f"/content/widget/{t['id']}.md"
        else:
            file_name = f"/content/widget/{t['id']}.{lang}.md"
        with open(current_directory + file_name, "w") as f:
            f.write((json.dumps(front_matter, indent=2)))
            f.write("\n")
            f.write("\n")
            f.write("{{< towns_widgets >}}")

        front_matter = {"layout": "single-page", "town": t}
        if lang == "en":
            file_name = f"/content/forecast/{t['id']}.md"
            front_matter["title"] = f"Forecasts for {t['name']}"
        else:
            if lang == "de":
                front_matter["title"] = f"Vorhersagen für {t['name']}"
            else:
                front_matter["title"] = f"Previsioni per {t['name']}"
            file_name = f"/content/forecast/{t['id']}.{lang}.md"
        with open(current_directory + file_name, "w") as f:
            f.write((json.dumps(front_matter, indent=2)))
            f.write("\n")
            f.write("\n")
            f.write(
                '{{< fullw_table pagename="forecast" >}}{{< forecast_chart >}}{{< /fullw_table >}}'
            )

    for s in venues_3:
        if args.dev:
            if s["id"] not in default_stations:
                continue
        rearrange_neighbors(s["neighbors"], venues_2, venues_3, venues_6)
        front_matter = {"layout": "single-page", "station": s}
        if lang == "en":
            file_name = f"/content/observation/{s['id']}.md"
            front_matter["title"] = f"Weather station {s['name']}"
        else:
            if lang == "de":
                front_matter["title"] = f"Wetterstation {s['name']}"
            else:
                front_matter["title"] = f"Stazione meteo {s['name']}"
            file_name = f"/content/observation/{s['id']}.{lang}.md"
        with open(current_directory + file_name, "w") as f:
            f.write((json.dumps(front_matter, indent=2)))
            f.write("\n")
            f.write("\n")
            f.write("{{< observation_chart >}}")

    for w in venues_6:
        if args.dev:
            if w["id"] not in default_webcams:
                continue
        associatedwebcams = [aw for aw in venues_7 if aw["id_region"] == w["id"]]
        rearrange_neighbors(w["neighbors"], venues_2, venues_3, venues_6)
        front_matter = {
            "layout": "single-page",
            "webcam": w,
            "associatedwebcams": associatedwebcams,
        }
        if lang == "en":
            file_name = f"/content/webcam/{w['id']}.md"
            front_matter["title"] = f"Webcam for {w['name']} ({w['elevation']} m)"
        else:
            if lang == "de":
                front_matter["title"] = f"Webcam für {w['name']} ({w['elevation']} m)"
            else:
                front_matter["title"] = f"Webcam per {w['name']} ({w['elevation']} m)"
            file_name = f"/content/webcam/{w['id']}.{lang}.md"
        current_directory = os.getcwd()
        with open(current_directory + file_name, "w") as f:
            f.write((json.dumps(front_matter, indent=2)))
            f.write("\n")
            f.write("\n")
            f.write("{{< webcam >}}")

    for w in venues_7:
        if args.dev:
            if w["id"] not in default_webcams:
                continue
        mainwebcam = [aw for aw in venues_6 if aw["id"] == w["id_region"]]
        w["neighbors"] = mainwebcam[0]["neighbors"]
        associatedwebcams = [
            aw
            for aw in venues_7
            if aw["id_region"] == w["id_region"] and aw["id"] != w["id"]
        ] + mainwebcam
        front_matter = {
            "layout": "single-page",
            "webcam": w,
            "associatedwebcams": associatedwebcams,
        }
        if lang == "en":
            file_name = f"/content/webcam/{w['id']}.md"
            front_matter["title"] = f"Webcam for {w['name']} ({w['elevation']} m)"
        else:
            if lang == "de":
                front_matter["title"] = f"Webcam für {w['name']} ({w['elevation']} m)"
            else:
                front_matter["title"] = f"Webcam per {w['name']} ({w['elevation']} m)"
            file_name = f"/content/webcam/{w['id']}.{lang}.md"
        current_directory = os.getcwd()
        with open(current_directory + file_name, "w") as f:
            f.write((json.dumps(front_matter, indent=2)))
            f.write("\n")
            f.write("\n")
            f.write("{{< webcam >}}")
