{
  description = "Tinia website";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  nixConfig = {
    # this configure the prompt when executing "nix develop"
    bash-prompt = "[\\t $(basename $ENV_ROOT):\${PWD#$ENV_ROOT}]\\$ ";
  };

  outputs = { self, flake-utils, gitignore, nixpkgs, nixos-generators }:
    let
      inherit (flake-utils.lib) simpleFlake;
      inherit (gitignore.lib) gitignoreFilterWith;

      name = "tinia-website";
      version = "0.1";
      overlay = final: prev:
        let
          pkgs = final;
          dependencies = rec {
            build = common;
            common = with pkgs; [ hugo ];
            dev = (with pkgs; [
              gnumake
              nixos-rebuild
              nodejs
              yarn2nix
            ]) ++ common;
          };
          # redefine ignore function to exclude some nix bits so that the
          # packages arent rebuilt everytime some vm config is changed
          gitignoreSourceWith = path:
            pkgs.lib.cleanSourceWith {
              name = "tinia-src";
              filter = gitignoreFilterWith {
                basePath = path.origSrc or path;
                extraRules = ''
                  ./nix
                  flake.*
                '';
              };
              src = path;
            };
          pkgsSrc = gitignoreSourceWith ./.;
          localPkgs = pkgs.callPackage ./nix/pkgs {
            inherit name version;
            src = pkgsSrc;
          };
        in {
          # simpleFlake looks for a mapping with the attribute equal to the passed
          # in `name` arg
          ${name} = rec {
            inherit (localPkgs) demoData jsDeps site;
            defaultPackage = site;
            devShell = pkgs.mkShell {
              name = "${name}-dev-shell";
              buildInputs = dependencies.dev;
              JS_DEPS = jsDeps;
              shellHook = ''
                export ENV_ROOT=$PWD
              '';
            };
            # this is just a workaround to redirect the HTTP and SSH ports of
            # the "testvm"
            runTestVM = pkgs.writeShellScriptBin "run-test-vm" ''
              export QEMU_NET_OPTS="hostfwd=tcp::8080-:8080,hostfwd=tcp::2222-:22"
              exec ${self.packages.x86_64-linux.testvm}/bin/run-meteo-vm
            '';
          };
        };
      testUsers = {
        mutableUsers = false;
        users = {
          root = {
            password = "root";
          };
          user = {
            password = "user";
            isNormalUser = true;
            extraGroups = [ "wheel" ];
          };
        };
      };
      prodUsers = {
        mutableUsers = false;
        users = {
          root = {
            password = "root";
          };
        };
      };
      testVMConf = {
        system = "x86_64-linux";
        specialArgs = {};
        modules = [
          ({config, ...}: {
            # just because most of te users testing the vm will have such keymap
            console.keyMap = "it";
            nixpkgs.overlays = [ overlay ];
            services.openssh.settings.PasswordAuthentication = true;
            virtualisation.vmVariant.virtualisation.diskSize = 4096;
            tinia.website = {
              installData = true;
              enableDev = true;
            };
            users = testUsers;
          })
          ./nix/config/vm.nix
        ];
      };
      prodVMConf = {
        system = "x86_64-linux";
        specialArgs = {};
        modules = [
          ({config, ...}: {
            networking = {
              useDHCP = false;
              interfaces.ens32 = {
                useDHCP = false;
                ipv4.addresses = [{
                  address = "195.231.68.251";
                  prefixLength = 24;
                }];
              };
              defaultGateway = {
                address = "195.231.68.1";
                interface = "ens32";
              };
              nameservers = [ "8.8.8.8" ];
            };
            nixpkgs.overlays = [ overlay ];
            tinia.monitorHost = "79.1.207.41";
            tinia.website = {
              # basicAuthFile = "/var/users";
              siteDomain = "meteo.report";
              forceSSL = true;
              installData = false;
              enableDev = false;
              # Enable only on the machine listening on "meteo.report"
              enableACME = true;
              tmsDownloadType = "complete";
              tmsStamp = "ae126ea79c30a2e268ee38edc86e19e8656fd35a2fad18743ccf039c90f4eda8";
            };
            # To alter when we've defined the actual users
            users = prodUsers;
          })
          ./nix/config/vm.nix
          ./nix/config/running-host.nix
        ];
      };
    in (simpleFlake {
      inherit self name nixpkgs overlay;
      systems = flake-utils.lib.defaultSystems;
    }) // {
      nixosConfigurations = {
        "meteo.report" = nixpkgs.lib.nixosSystem prodVMConf;
        "testvm" = nixpkgs.lib.nixosSystem testVMConf;
      };
      packages.x86_64-linux = let
        mkImage = format: cfg:
          nixos-generators.nixosGenerate ({
            inherit format;
          } // cfg);
      in {
        # This is provided directly by nixpkgs and generates a VM and a script
        # to spawn it with qemu/kvm on GNU/Linux
        testvm = self.nixosConfigurations.testvm.config.system.build.vm;
        # These are provided by the `nixos-generate` input and just build the
        # production and test configuration into a disk image compatible with
        # the platform
        prodvm-vbox = mkImage "virtualbox" prodVMConf;
        prodvm-vmware = mkImage "vmware" prodVMConf;
        testvm-vbox = mkImage "virtualbox" testVMConf;
        testvm-vmware = mkImage "vmware" testVMConf;
      };
    };
}
