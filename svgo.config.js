module.exports = {
  multipass: true,
  plugins: [
    {
      name: 'preset-default',
      params: {
        overrides: {
          convertPathData: false,
          mergePaths: false,
          removeViewBox: false,
        },
      },
    },
    'removeDimensions'
  ],
};
