#!/bin/sh
set -e
find public/widget/* public/de/widget/* public/it/widget/* -type f \
  ! -wholename "*/c4efef40-bb6f-4cd3-a982-4e7579da701e*" \
  ! -wholename "*/e0c04e2d-8221-48d3-92b9-56e26e743213*" \
  ! -wholename "*/5d9e12bb-7274-483e-9acd-44bfdcb916e5*" \
  -delete
find public/forecast/* public/de/forecast/* public/it/forecast/* -type f \
  ! -wholename "*/c4efef40-bb6f-4cd3-a982-4e7579da701e*" \
  ! -wholename "*/e0c04e2d-8221-48d3-92b9-56e26e743213*" \
  ! -wholename "*/5d9e12bb-7274-483e-9acd-44bfdcb916e5*" \
  -delete
find public/observation/* public/de/observation/* public/it/observation/* -type f \
  ! -wholename "*/2fb599de-dcc7-4424-8410-c7205be0e6e3*" \
  ! -wholename "*/65a91ab5-032b-40ea-a03f-42773d60ec34*" \
  ! -wholename "*/f439b2f0-c5b9-43f4-b058-f407f7c99141*" \
  -delete
find public/webcam/* public/de/webcam/* public/it/webcam/* -type f \
  ! -wholename "*/72eb53f3-2596-4eaa-8f17-6ea6b51f2a8c*" \
  ! -wholename "*/471df0bb-c4de-4cd5-b74d-a1bcb445f8ba*" \
  ! -wholename "*/c8808cf3-77b1-4da8-b312-d072d4a922b7*" \
  -delete
