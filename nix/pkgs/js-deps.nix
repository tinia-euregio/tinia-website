# -*- coding: utf-8 -*-
# :Project:   tinia-website — JavaScript and CSS assets
# :Created:   gio 14 dic 2023, 15:39:40
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023 simevo s.r.l.
#

{mkYarnModules, name, src, version}: mkYarnModules {
  pname = "${name}-js-deps";
  inherit version;
  packageJSON = src + "/package.json";
  yarnLock = src + "/yarn.lock";
  yarnNix = src + "/yarn.nix";
}
