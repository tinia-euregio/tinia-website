# -*- coding: utf-8 -*-
# :Project:   tinia-website — packages collector
# :Created:   gio 14 dic 2023, 15:29:08
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023 simevo s.r.l.
#

{pkgs, name ? "tinia-website", src ? ./.., version ? "0.1"}:
  let
    inherit (pkgs) callPackage;
  in rec {
    demoData = callPackage ./demo-data.nix {inherit name src version;};
    jsDeps = callPackage ./js-deps.nix {inherit name src version;};
    site = callPackage ./site.nix {inherit jsDeps name src version;};
 }
