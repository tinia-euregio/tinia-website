# -*- coding: utf-8 -*-
# :Project:   tinia-website — Website package
# :Created:   gio 14 dic 2023, 15:34:30
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023, 2024 simevo s.r.l.
#

{hugo, jsDeps, name, python3, src, stdenvNoCC, version}: stdenvNoCC.mkDerivation {
  inherit name src version;
  buildInputs = [ hugo python3 ];
  JS_DEPS = jsDeps;
  makefile = "Makefile.nix";
}
