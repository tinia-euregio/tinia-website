# -*- coding: utf-8 -*-
# :Project:   tinia-website — Demo data package
# :Created:   gio 14 dic 2023, 15:43:59
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023 simevo s.r.l.
#

{name, runCommand, src, version}:
  runCommand "${name}-demo-data-${version}" {} ''
    mkdir $out
    cp -r "${src}/data/." $out
  ''
