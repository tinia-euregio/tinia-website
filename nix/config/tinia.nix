# -*- coding: utf-8 -*-
# :Project:   tinia-website — System config module for the website
# :Created:   gio 14 dic 2023, 15:27:29
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023, 2024 simevo s.r.l.
#

{pkgs, config, lib, ...}:
  let
    inherit (lib) mkEnableOption mkOption mkPackageOption optional
      optionalAttrs optionalString types;
    websitePkgs = pkgs.tinia-website;
  in {
    options.tinia =  {
      monitorHost = mkOption {
        description = ''
          The IP of the host which is the origin of the scraping of the metrics.
        '';
        default = null;
        type = with types; nullOr str;
      };
      website = {
        basicAuthFile = mkOption {
          description = ''
            Optional basic Auth password file. Can be created via:
            htpasswd -Bc <filename> <username>.
          '';
          default = null;
          type = with types; nullOr path;
        };
        sitePkg = mkPackageOption websitePkgs "site" {};
        dataPkg = mkPackageOption websitePkgs "demoData" {};
        enableDev = mkEnableOption "Enable development config";
        enableACME = mkEnableOption "Letsencrypt certificate request";
        forceSSL = mkEnableOption "force use of HTTPS protocol";
        installData = mkEnableOption "install data from the data package";
        siteAliases = mkOption {
          description = "Additional site aliases";
          default = [];
          type = with types; listOf str;
        };
        siteDomain = mkOption {
          description = "The domain used to reach the website";
          default = "meteo.report";
          type = types.str;
        };
        siteLocation = mkOption {
          description = "Location of the site on the filesystem";
          default = "/srv/tinia-website";
          type = types.path;
        };
        siteVarLocation = mkOption {
          description = "Location of the site on the filesystem";
          default = "/var/lib/tinia-website";
          type = types.path;
        };
        siteVarUser = mkOption {
          description = ''
            Owner of the 'var' directory, this is the same user that will do the
            sync from the manager instance.
          '';
          default = "tinia";
          type = types.str;
        };
        tmsStamp = mkOption {
          description = ''
            Stamp value for TMS tiles, change it to activate
            download. If the `tmsDownloadType` is `complete` this should be the
            sha256 checksum of the tarball.
          '';
          default = "20240410-01";
          type = types.str;
        };
        tmsDownloadType = mkOption {
          description = ''
            "partial" to download only few layers, "complete" to download them all
          '';
          default = "partial";
          type = types.enum [ "partial" "complete" ];
        };
      };
    };
    config =
      let
        cfg = config.tinia.website;
        tmpDownAll = pkgs.writeShellApplication {
          name = "tinia-tms-down-complete.sh";
          runtimeInputs = with pkgs; [ coreutils findutils wget ];
          text = builtins.readFile ./get_tms_all.sh;
        };
        tmpDownAllTar = pkgs.writeShellApplication {
          name = "tinia-tms-down-complete-tar.sh";
          runtimeInputs = with pkgs; [ coreutils curl gnutar xz ];
          text = builtins.readFile ./get_tms_all_tar.sh;
        };
        tmpDownSome = pkgs.writeShellApplication {
          name = "tinia-tms-down-partial.sh";
          runtimeInputs = with pkgs; [ coreutils curl ];
          text = builtins.readFile ./get_tms.sh;
        };
        updateTMS = pkgs.writeShellApplication {
          name = "update-tinia-tms.sh";
          runtimeInputs = [tmpDownSome tmpDownAllTar];
          text = ''
            HOME_TMS_STAMP="$(cat .tms_stamp || echo 0)"
            NEW_TMS_STAMP="${cfg.tmsStamp}"
            if [ "$HOME_TMS_STAMP" != "$NEW_TMS_STAMP" ]; then
              echo "Downloading TMS layers: ${cfg.tmsDownloadType}"
              echo "Working directory is $PWD"
              ${if cfg.tmsDownloadType == "partial"
                then "${tmpDownSome}/bin/tinia-tms-down-partial.sh"
                else ''
                  ${tmpDownAllTar}/bin/tinia-tms-down-complete-tar.sh
                  if [ "$(cat .downloaded_sha256 || echo 0)" != "$NEW_TMS_STAMP" ]; then
                    echo "Wrong sha256"
                    exit 10
                  fi
                ''
               }
              chmod -R u=rwX,go=rX tms
              echo -n "${cfg.tmsStamp}" > .tms_stamp
              echo "Saved stamp ${cfg.tmsStamp}"
            else
              echo "Stamp is ok, not downloading TMS layers"
            fi
          '';
        };
      in {
        security.acme = lib.mkIf cfg.enableACME {
          acceptTerms = true;
          defaults.email = "info@simevo.com";
        };
        services = {
          nginx = {
            enable = true;
            recommendedGzipSettings = true;
            recommendedOptimisation = true;
            virtualHosts =
              let
                maybeAddTestCerts = virtualHost: virtualHost // (optionalAttrs (
                  cfg.forceSSL && !cfg.enableACME) {
                    sslCertificate = "${pkgs.path}/nixos/tests/common/acme/server/acme.test.cert.pem";
                    sslCertificateKey = "${pkgs.path}/nixos/tests/common/acme/server/acme.test.key.pem";
                  });
                commonRedirectConfig = addendum: {
                  inherit (cfg) enableACME;
                  default = false;
                  serverName = "www.${cfg.siteDomain}";
                  globalRedirect = "${cfg.siteDomain}";
                } // (addendum);
              in {
                "${cfg.siteDomain}" = maybeAddTestCerts {
                  inherit (cfg) basicAuthFile enableACME forceSSL;
                  default = true;
                  extraConfig = ''
                    location ~ ^/(de/widget|it/widget|widget)/ {
                        charset utf-8;
                    }
                  '';
                  listen = optional cfg.enableDev {addr = "*"; port = 8080;} ;
                  locations."/var" = {
                    root = cfg.siteVarLocation;
                  };
                  serverAliases = cfg.siteAliases
                                  ++ (optional cfg.enableDev "localhost");
                  serverName = cfg.siteDomain;
                  root = cfg.siteLocation;
                };
                "www_${cfg.siteDomain}_http" = commonRedirectConfig {};
                "www_${cfg.siteDomain}_https" = lib.mkIf
                  (cfg.forceSSL || cfg.enableACME) (
                    commonRedirectConfig (
                      maybeAddTestCerts {
                    onlySSL = true;
                  }));
              };
          };
        };
        system.activationScripts.tinia-website = ''
          mkdir -p ${cfg.siteLocation} ${cfg.siteVarLocation}
          cp -r ${cfg.sitePkg}/. ${cfg.siteLocation}
          cd ${cfg.siteVarLocation}
          [ ! -e var ] && ln -sr . var
          ${optionalString cfg.installData ''
            cp -r ${cfg.dataPkg}/. data
          ''}
          chown -R ${cfg.siteVarUser} ${cfg.siteVarLocation}
          chmod -R u=rwX,go=rX ${cfg.siteLocation} ${cfg.siteVarLocation}
        '';
        systemd.services = {
          update-tms = {
            enable = true;
            after = [ "network-online.target" ];
            restartIfChanged = true;
            serviceConfig =  {
              RemainAfterExit = true;
              Type = "oneshot";
              WorkingDirectory = "${cfg.siteLocation}";
            };
            script = "${updateTMS}/bin/update-tinia-tms.sh";
            wants = [ "network-online.target" ];
            wantedBy = [ "nginx.service" ];
          };
        };
    };
  }
