# -*- coding: utf-8 -*-
# :Project:   tinia-website — Additional configuration for the running host
# :Created:   mar 19 dic 2023, 19:20:45
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023 simevo s.r.l.
#

{config, pkgs, ...}: {

  fileSystems."/" = {
    device = "/dev/disk/by-label/nixos";
    autoResize = true;
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/ESP";
    fsType = "vfat";
  };

  boot.growPartition = true;

  boot.loader.grub = {
    device = "nodev";
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  virtualisation.vmware.guest.enable = true;
}
