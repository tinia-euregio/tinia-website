#!/usr/bin/env sh

set -e

wget -r -nv -np --reject-regex '^.*\.png' https://static.avalanche.report/tms/7/
wget -r -nv -np --reject-regex '^.*\.png' https://static.avalanche.report/tms/8/
wget -r -nv -np --reject-regex '^.*\.png' https://static.avalanche.report/tms/9/
wget -r -nv -np --reject-regex '^.*\.png' https://static.avalanche.report/tms/10/
wget -r -nv -np --reject-regex '^.*\.png' https://static.avalanche.report/tms/11/

find static.avalanche.report/ -name '*.html*' -delete

rm -rf tms
mv static.avalanche.report/tms .
rmdir static.avalanche.report
