# -*- coding: utf-8 -*-
# :Project:   tinia-website — VM main configuration
# :Created:   gio 14 dic 2023, 17:25:41
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2023, 2024 simevo s.r.l.
#

{config, modulesPath, lib, pkgs, ...}: {

  imports = [
    "${toString modulesPath}/profiles/minimal.nix"
    ./tinia.nix
  ];

  boot.tmp.cleanOnBoot = lib.mkDefault true;

  environment = {
    systemPackages = with pkgs; [
      apacheHttpd # needed for htpasswd
      htop
      rsync
      tree
      zile
    ];
    # See https://github.com/NixOS/nixpkgs/issues/119841
    noXlibs = false;
  };
  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  networking = {
    domain = "report";
    firewall = {
      enable = true;
      allowedTCPPorts = [ 22 80 443 ]
        ++ pkgs.lib.optional config.tinia.website.enableDev 8080;
      allowedUDPPorts = [ 53 ];
    };
    hostName = "meteo";
  };

  nix = {
    extraOptions = ''
      min-free = ${toString (2 * 1024 * 1024)}
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 15d";
    };
    nixPath = [
      "nixpkgs=${pkgs.path}"
      "/nix/var/nix/profiles/per-user/root/channels"
    ];
    settings = {
      auto-optimise-store = true;
      experimental-features = "nix-command flakes";
      trusted-users = ["@wheel"];
    };
  };

  # Enable the OpenSSH daemon.
  services = {
    logrotate = {
      enable = true;
      settings = {
        nginx.frequency = "daily";
      };
    };
    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = lib.mkDefault false;
      };
    };
    nginx = {
      enable = true;
      statusPage = true;
    };
    prometheus.exporters =
      let
        monitorHost = config.tinia.monitorHost;
        isMonitoringEnabled = monitorHost != null;
      in lib.optionalAttrs isMonitoringEnabled {
        node = {
          enable = true;
          openFirewall = true;
          firewallFilter = "-p tcp -m tcp --dport 9100";
          enabledCollectors = [ "systemd" ];
        };
        nginx = {
          enable = true;
          openFirewall = true;
          firewallFilter = "-p tcp -m tcp --dport 9113";
          sslVerify = false;
        };
    };
  };

  system = {
  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # copySystemConfiguration = true;
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  stateVersion = "23.11"; # Did you read the comment?
  };

  # Set your time zone.
  time.timeZone = "Europe/Rome";

  # Inject the ssh public key
  users.users =
    let
      admins = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKADi+jUrRSZfmXqVOWCo7VAL9Cfvuj2Reom6AOZgqtY tinia@frontend2"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINQ+3LybQIrxFu0TT2LumEFP6XFVpYaEpYaeEEgiLeOf azazel@bean"
      ];
    in {
      root.openssh.authorizedKeys.keys = admins;
      tinia = {
        password = "tinia";
        isNormalUser = true;
        extraGroups = [ ];
        openssh.authorizedKeys.keys = admins ++ [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICO3S877tjOtfd3Bs0Pau4IEo+SY4tbtpLKo5d3583uy manager@tinia-manager"
        ];
      };
    };
}
