#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# :Project:   tinia-website — tms layers downloader
# :Created:   mer 24 apr 2024, 21:45:47
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU Affero General Public License version 3 or later
# :Copyright: © 2024 simevo s.r.l.
#

SRC="https://tinia.simevo.com/tms.tar.xz"

curl -s -o - "$SRC" | tee \
    >(sha256sum | sed -z 's/ *-\n//' > .downloaded_sha256) |\
    tar -xJf -
