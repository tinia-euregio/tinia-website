#!/usr/bin/env sh

set -e

mkdir -p tms/8/132
mkdir -p tms/8/133
mkdir -p tms/8/134
mkdir -p tms/8/135
mkdir -p tms/8/136
mkdir -p tms/8/137
mkdir -p tms/8/138
mkdir -p tms/8/139

curl -o tms/8/132/89.webp https://static.avalanche.report/tms/8/132/89.webp
curl -o tms/8/132/90.webp https://static.avalanche.report/tms/8/132/90.webp
curl -o tms/8/132/91.webp https://static.avalanche.report/tms/8/132/91.webp

curl -o tms/8/133/89.webp https://static.avalanche.report/tms/8/133/89.webp
curl -o tms/8/133/90.webp https://static.avalanche.report/tms/8/133/90.webp
curl -o tms/8/133/91.webp https://static.avalanche.report/tms/8/133/91.webp

curl -o tms/8/134/89.webp https://static.avalanche.report/tms/8/134/89.webp
curl -o tms/8/134/90.webp https://static.avalanche.report/tms/8/134/90.webp
curl -o tms/8/134/91.webp https://static.avalanche.report/tms/8/134/91.webp

curl -o tms/8/135/89.webp https://static.avalanche.report/tms/8/135/89.webp
curl -o tms/8/135/90.webp https://static.avalanche.report/tms/8/135/90.webp
curl -o tms/8/135/91.webp https://static.avalanche.report/tms/8/135/91.webp

curl -o tms/8/136/89.webp https://static.avalanche.report/tms/8/136/89.webp
curl -o tms/8/136/90.webp https://static.avalanche.report/tms/8/136/90.webp
curl -o tms/8/136/91.webp https://static.avalanche.report/tms/8/136/91.webp

curl -o tms/8/137/89.webp https://static.avalanche.report/tms/8/137/89.webp
curl -o tms/8/137/90.webp https://static.avalanche.report/tms/8/137/90.webp
curl -o tms/8/137/91.webp https://static.avalanche.report/tms/8/137/91.webp

curl -o tms/8/138/89.webp https://static.avalanche.report/tms/8/138/89.webp
curl -o tms/8/138/90.webp https://static.avalanche.report/tms/8/138/90.webp
curl -o tms/8/138/91.webp https://static.avalanche.report/tms/8/138/91.webp

curl -o tms/8/139/89.webp https://static.avalanche.report/tms/8/139/89.webp
curl -o tms/8/139/90.webp https://static.avalanche.report/tms/8/139/90.webp
curl -o tms/8/139/91.webp https://static.avalanche.report/tms/8/139/91.webp
