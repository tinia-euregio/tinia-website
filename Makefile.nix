# -*- coding: utf-8 -*-
# :Project:   tinia-website — Nix automation rules
# :Created:   sab 5 mar 2022, 02:10:20
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2022-2024 Simevo s.r.l.
#

out ?= build

.PHONY: build
build: assets
	python gen_pages.py
	hugo

.PHONY: install
install:
	mkdir -p $(out)
	cp -r public/* $(out)

.PHONY: yarn
yarn: yarn.nix

yarn.nix: package.json yarn.lock
	yarn2nix > $@

.PHONY: assets
assets: node_modules
	$(MAKE) all

node_modules:
	ln -sf $(JS_DEPS)/node_modules .

.PHONY: build_production_config
build_production_config:
	nixos-rebuild build --flake '.#meteo.report'

.PHONY: update_production_config
update_production_config:
	nixos-rebuild switch --flake '.#meteo.report' \
	--target-host root@195.231.68.251

.PHONY: rollback_production_config
rollback_production_config:
	nixos-rebuild switch --flake '.#meteo.report' \
	--target-host root@195.231.68.251 --rollback

.PHONY: run_test_vm
run_test_vm:
	nix build .#runTestVM
	@echo "Once started you can ssh into the VM with ssh -p 2222 user@localhost, pw: user"
	@echo "You can also access the website by pointing your browser to the URI http://localhost:8080"
	./result/bin/run-test-vm
