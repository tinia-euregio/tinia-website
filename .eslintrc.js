module.exports = {
  root: true,
  env: {
    browser: true,
    es2017: true
  },
  plugins: [
    "html",
    "@eladavron/jinja"
  ],
  rules: {
    "no-useless-escape": "off",
  },
  extends: [
    'eslint:recommended',
  ],
  globals: {
    Alpine: "readonly",
    Chart: "readonly",
    L: "readonly",
    self: "writable",
  },
}
